# README #

This project uses Udemy API to process a course based on its course id. Grabs data from 2 Rest API sources, puts it together downloads images, downloads and beautifies JSON files, and produces a summary text file describing course info like title, authors, chapters and lectures. Also uses image-to-ascii to display a image from url as ascii on console.

It also installs locally npm dependencies on the fly, uses them and at the end deletes the node_modules folder and package-lock.json file, thus keeping the folder clean.

I've added sort of progress indicator for the https.get *data* stage, with outputting . for every two chunks received. 

For now the script is meant to suplement [Udeler](https://github.com/FaisalUmair/udemy-downloader-gui) which downloads owned/purchased Udemy courses, or to just demonstrate JSON / API parsing and local file saving of text and binary data. In the future I may add more functionality and of course improve the code.

### What is this repository for? ###

* Process Udemy Course data based on course ID

### How do I get set up? ###

* Node.js (I've used v10.21.0, but in theory should work with newer and some odler versions too - it uses mostly common Node.js modules)
* For the ASCII art part (jpg -> ascii), I am using [image-to-ascii](https://github.com/IonicaBizau/image-to-ascii), but in theory you don't have to install it at all, as part of my project code is to demonstrate how you can use Node.js to dynamically install node modules needed, and cleanup at the end. Since there aren't many external modules used, it's a quick job.
* [image-to-ascii](https://github.com/IonicaBizau/image-to-ascii) does use a global dependency (not Node.js one) called **graphicsmagick** - it's a one off process you can do on either Ubuntu / Mac, for other install options search for *graphicsmagick* for your operating system. For Windows it would be best to use Linux Subsystem - [WSL](https://docs.microsoft.com/en-us/windows/wsl/install-win10).

```
sudo apt-get install graphicsmagick # Ubuntu
brew install graphicsmagick         # Mac OS X
```

* Once everything is installed, place the *udemy-process.js* ina  folder (for example in a folder where Udeler has downloaded one of the courses you own), and on a command line run
```
node udemy-process courseID # courseID is the ID of your Udemy course - for example 1329100
```
* How do can you get course ID of a Udemy course. The easiest way is to open the main course page, for example [https://www.udemy.com/course/progressive-web-app-pwa-the-complete-guide/](https://www.udemy.com/course/progressive-web-app-pwa-the-complete-guide/), open developer toolbars, go to Network connections and apply a filter of `api-2.0/course-landing-components`, the look at the request URL, for example:
```
https://www.udemy.com/api-2.0/course-landing-components/1329100/me/?components=buy_button,discount_expiration,gift_this_course,purchase,deal_badge,redeem_coupon
```
and the numeric part after `api-2.0/course-landing-components` - **`1329100`** in this case - is the course id. Use it and pass it to the node command line, in this example:

```
node udemy-process 1329100
```

### Example output ###

See inside the `./example-output` folder in this repo - note this folder will not be created by the script, all files created by the script go in the same folder as the script itself, it's just merely a copy of example output put in a folder for the purpose of the repo. When you checkout the repo, feel free to remove this folder, all you need is just the main `udemy-process.js` file.