'use strict';

// example run: 
// node udemy-process 1329100
// where 1329100 is course id

const fs = require('fs');
const path = require('path');
const https = require('https');
const exec = require('child_process').exec;

//Base output file names on current folder name
const outputFileName = path.basename(path.resolve()).replace(/[^a-z0-9]/gi, '_').replace(/___/gi, '_').replace(/__/gi, '_').toLowerCase();

exec('npm install --save image-to-ascii', (error, stdout, stderr) => { //install first if missing then run the rest, at the end we'll remove the node_modules and associated files
    const img2ascii = require('image-to-ascii');

    let courseData, courseChaptersData;

    const saveFromURL = (url, localPath, callback) => {
        https.get(url, (response) => {
            response.pipe(fs.createWriteStream(localPath))
            .on('close', callback);
        });
    };

    const saveJSONFromURL = (url, localPath, taskDescription, callback) => {
        https.get(url, (resp) => {
            let data = '';
            let parsedData = '';
            let progressCounter = 0;

            process.stdout.write('Download for ' + taskDescription + ' in progress ');

            // A chunk of data has been recieved.
            resp.on('data', (chunk) => {
                !(progressCounter%2) && process.stdout.write('.'); //progress indicate on every 2nd chunk
                data += chunk;
                progressCounter++;
            });

            // The whole response has been received. Save the result.
            resp.on('end', () => {
                console.log('\r\nDownload for ' + taskDescription + ' finished! ✅');

                parsedData = JSON.parse(data);

                if (localPath.indexOf('curriculum') > -1) {
                    courseChaptersData = parsedData;
                } else {
                    courseData = parsedData;
                }

                //Save to file
                fs.writeFileSync(localPath, JSON.stringify(parsedData, null, 4)); //pretty json

                //Callback
                callback();
            });

        }).on("error", (err) => {
            console.log("Error: " + err.message);
        });
    };

    const secondsToTime = (seconds) => {
        const isoStr = new Date(seconds * 1000).toISOString();
        const daysNum = (parseInt(isoStr.substr(8, 2)) - 1);
        const daysStr = daysNum > 0 ? daysNum + ' day' + (daysNum < 2 ? '' : 's') +' and ' : '';
        const hmsStr = isoStr.substr(11, 8);
        return daysStr + hmsStr;
    };

    //Course Main JSON download
    saveJSONFromURL('https://www.udemy.com/api-2.0/courses/'+process.argv[2]+'/?fields[course]=id,title,headline,image_240x135,image_480x270,description,visible_instructors,objectives,target_audiences,created,avg_rating,num_lectures,prerequisites,url,estimated_content_length,content_length_video,published_time,last_update_date',
        outputFileName + '_course.json', 
        'Course Main JSON',
        () => {
            console.log('\r\nProcessing Course: "' + courseData.title + '"\r\n');

            //Course Curriculum JSON download
            saveJSONFromURL('https://www.udemy.com/api-2.0/course-landing-components/'+process.argv[2]+'/me/?components=curriculum',
                outputFileName + '_curriculum.json',
                'Course Curriculum JSON',
                () => {

                    //Image download
                    saveFromURL(courseData.image_480x270, outputFileName + '.jpg', () => console.log('Downloading Course Thumb done! ✅'));

                    // Convert an octocat into ascii :)
                    img2ascii(courseData.image_240x135, {size: {height: 25}}, (err, asciiImgResult) => {
                        console.log(asciiImgResult);
                    })

                    //Process JSON data to save summary file
                    let courseInfoText = '';
                    let tempPlayVar = '';

                    courseInfoText += `Course ID: ${courseData.id}\r\n\r\n`;
                    courseInfoText += `Course URL: https://udemy.com${courseData.url}\r\n\r\n`;
                    courseInfoText += `Course Thumb: ${courseData.image_480x270}\r\n\r\n`;
                    courseInfoText += `Course Title: ${courseData.title}\r\n\r\n`;
                    courseInfoText += `Course Author(s):\r\n\t- ${courseData.visible_instructors.filter(instructor => instructor._class === 'user').map(instructor => instructor.display_name).join('\r\n\t- ')}\r\n\r\n`;
                    courseInfoText += `Course Headline: ${courseData.headline}\r\n\r\n`;

                    console.log('\r\nChapter and Lecture in progress...')

                    tempPlayVar = '';
                    courseChaptersData.curriculum.data.sections.forEach((chapter) => {
                        let chapterInfo = chapter.title + ' [' + chapter.content_length_text + ']';
                        
                        console.log('> ' + chapterInfo);
                        tempPlayVar += '\r\n  ' + chapterInfo;
                        
                        chapter.items.forEach((lecture) => {
                            let lectureInfo = lecture.title + ' [' + lecture.content_summary + ']';
                            
                            console.log('    - ' + lectureInfo);
                            tempPlayVar += '\r\n\t- ' + lectureInfo;
                        });

                        tempPlayVar += '\r\n';
                    })

                    console.log('\r\nChapter and Lecture processing finished! ✅\r\n')

                    courseInfoText += `Course Length: ${secondsToTime(courseData.content_length_video)}\r\n\r\n`;
                    courseInfoText += `Course Last Updated: ${new Date(courseData.last_update_date).toDateString()}\r\n\r\n`;
                    courseInfoText += `Course Chapters / Lectures:${tempPlayVar}\r\n`; //chapters               
                    courseInfoText += `Course Objectives:\r\n\t- ${courseData.objectives.join('\r\n\t- ')}\r\n\r\n`;
                    courseInfoText += `Course Audience:\r\n\t- ${courseData.target_audiences.join('\r\n\t- ')}\r\n\r\n`;

                    tempPlayVar = `Course Description:\r\n${'_'.repeat('Course Description:'.length)}\r\n\r\n${courseData.description.replace(/<p>/ig,'<p>\r\n\r\n').replace(/<br>/ig,'<br>\r\n').replace(/<[^>]*>?/gm,'').replace(/\!/ig,'! ').replace(/\?/ig,'? ').replace(/\:/ig,': ').replace(/&nbsp;/ig,' ').replace(/&amp;/ig,'&')}\r\n${'_'.repeat('Course Description:'.length)}\r\n\r\n`; //strip tags
                    courseInfoText += tempPlayVar.replace(/\r\n\r\n\r\n/ig,'\r\n'); //clear first line break(s)

                    fs.writeFileSync(outputFileName + '.txt', courseInfoText); //save course info

                    exec('rm -rf node_modules && rm package-lock.json', (error, stdout, stderr) => {}); //cleanup node_modules as it's served its purpose                    
                }
            );
        }
    );
});
